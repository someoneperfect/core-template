import React, { Component, lazy, Suspense } from 'react';
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Row,
  Table,
  Input,
  Form,
  FormGroup,
  Label,
  FormText,
  Modal, ModalBody, ModalFooter, ModalHeader
} from 'reactstrap';
import {
  Link
} from 'react-router-dom';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import Moment from 'moment';
import DatePicker from 'react-date-picker';
import CoreExcel from '../../../component/CoreExcel';

import superagent from 'superagent';

const flexCenter = {
  justifyContent: 'center'
};
const btn60px = {
  width: '60px'
};

class Base extends Component{
  constructor(props) {
    super(props);

    this.state = {
      url:'',
      title:'',
      keyword:'',
      date_from: '',
      date_to: '',
      colNameList:[],
      dataList:[],
      isView:false,
      isReport:false,
      searchModal:false,
      loadMore:false,
      dropdownOpen:new Array(19).fill(false),
      data:{},
      dataSets:[],
      summary : {
        total_amount : 0,
        hpp : 0,
        komisi : 0,
        labarugi : 0
      },
      confirmDeleteModal:false,
      itemToDelete:{}
    };
    this.toggle = this.toggle.bind(this);
    this.toggleSearchModal = this.toggleSearchModal.bind(this);
    this._saveData = this._saveData.bind(this);
    this._deleteData = this._deleteData.bind(this);
    this._getDataList = this._getDataList.bind(this);
    this._searchData = this._searchData.bind(this);
    this._afterSaveUpdateDelete = this._afterSaveUpdateDelete.bind(this);
    this.toggleConfirmDeleteModal = this.toggleConfirmDeleteModal.bind(this);
  }

  _searchData(){
    this.setState({
      dataList:[]
    });
    const dataSets=[];
    superagent
    .get(this.state.url)
    .query({
      keyword: this.state.keyword,
      date_from: this.state.date_from !== '' ? Moment(this.state.date_from).format("YYYY-MM-DD") : '',
      date_to: this.state.date_to !== '' ? Moment(this.state.date_to).format("YYYY-MM-DD") : ''
    })
    .then(res =>{
      if(res.body!=null){
        this.setState({
          dataList : res.body
        });
        if (this.state.isReport) {
          let totalAmount = 0, totalHPP = 0, totalCommision = 0, totalLabarugi = 0;
          res.body.map(data => {
            totalAmount += parseInt(data.total_amount);
            totalHPP += parseInt(data.hpp);
            totalCommision += parseInt(data.commision);
            totalLabarugi += parseInt(data.labarugi);
            this.setState(prevState => ({
              summary: {
                ...prevState.summary,
                total_amount: totalAmount,
                hpp: totalHPP,
                komisi: totalCommision,
                labarugi: totalLabarugi
              }
            }));
          });
        }
        const headerList =[];
        for(const i in res.body[0]){
          headerList.push([i]);
        }
        dataSets.push({
          dataset:this.state.dataList,
          sheet:'sheet 1',
          colNameList: headerList
        });
        this.setState({
          dataSets : dataSets
        })
      }
    });
  }

  _afterSaveUpdateDelete(){
  }

  _saveData(){
      var that = this;
      that.setState({
        data :{
          ... that.state.data,
          last_update_date : Moment(new Date()).format("YYYY-MM-DD HH:mm:ss")
        }
      })

      var newData = that.state.data;
      for(var propertyName in newData){
        if (newData[propertyName] !== null) {
          if(typeof newData[propertyName].getMonth === 'function' )
            newData[propertyName] = Moment(newData[propertyName]).format("YYYY-MM-DD")
        }
      }

      if(typeof that.state.data.id !== 'undefined' && that.state.data.id != 0){
        superagent.put(this.state.url)
        .set('Content-Type', 'application/json')
        .send(newData)
        .end(function(err,res){
          if(res.statusCode==200){
            window.alert("Berhasil Menyimpan Data");
            that._setInitialState();
            that._getDataList();
            that._afterSaveUpdateDelete();
          }
        });
      }else{
        superagent.post(this.state.url)
        .set('Content-Type', 'application/json')
        .send(newData)
        .end(function(err,res){
          if(res.statusCode==200){
            window.alert("Berhasil Menyimpan Data");
            that._setInitialState();
            that._getDataList();
            that._afterSaveUpdateDelete();
          }
        });
      }

  }

  _deleteData(data){
      var that = this;
      data.status_code= 0;
      data.status_name = 'DELETED';
      data.car_images = [];
      superagent.put(this.state.url)
      .set('Content-Type', 'application/json')
      .send(data)
      .end(function(err,res){
        if(res.statusCode==200){
          window.alert("Berhasil Menghapus Data");
          that.setState({
            data:{
              bulan :1,
              tahun : 2019
            },
            isView:false
          });
          that._getDataList();
          that._afterSaveUpdateDelete();
        }
      });

  }

  _createNew(){
      this._setInitialState();
      this.setState({isView:true});
  }

  componentDidMount(){
    this.setState({dataList:[]});
    this._getDataList();
  }

  _renderTableHeader = () => {
    var thisClass = this;
    return this.state.colNameList.map(function(data, index){
      return (
        <th>{data}</th>
      )
    })
  }

  _renderSearchModal = () =>{
    return(
      <div>
      </div>
    )
  }

  toggleSearchModal() {
    this.setState({
      searchModal: !this.state.searchModal,
    });
  }

  toggle(i) {
      const newArray = this.state.dropdownOpen.map((element, index) => { return (index === i ? !element : false); });
      this.setState({
        dropdownOpen: newArray,
      });
    }

  renderOptionIdValue(data){
    let items = [];
    for(var x in data){
      items.push(<option key={x} value={data[x].id}>{data[x].label}</option>);
    }
     return items;
  }

  renderOption(data){
    let items = [];
    for(var x in data){
      items.push(<option key={x} value={data[x].label}>{data[x].label}</option>);
    }
     return items;
  }

  renderFormTemplate(){
    return(
      <div>
        <Row>
          <Col>
            <div className="pull-left mb-3">
              <Button onClick={()=> {
                if (this.props.location.pathname === '/stock') {
                  this.setState({isView:false, isEdit:false, files:[]})
                } else {
                  this.setState({isView:false})
                }
              }} className="mr-2 btn btn-warning">Batal / Kembali</Button>
            </div>
          </Col>
        </Row>
        <Row>
          <Col>
            <Card>
              <CardHeader className="bg-yellow">
                <div className="pull-left d-flex">
                  <h3>{this.props.location.pathname === '/transaction' ? 'Informasi Transaksi' : 'Informasi Kendaraan'}</h3>
                </div>
              </CardHeader>
              <CardBody >
                  <div>
                    {this.renderForm()}
                  </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }

  renderHeaderList(){
    return(
      <div>
        <div className="pull-left d-inline-flex">
          <h3>{this.state.title}</h3>
        </div>
        <div className="pull-right d-inline-flex">
          <Input value={this.state.keyword} onChange={(e)=>{this.setState({
            keyword:e.target.value
          })}} className="mx-2"></Input>
          <Button className="btn btn-success" onClick={this._searchData}>Search</Button>
        </div>
      </div>
    )
  }

  renderListTemplate(){
    return(
      <div>
        <Row>
          <Col>
            <Card>
              <CardHeader className="bg-yellow">
                {this.renderHeaderList()}
              </CardHeader>
              <CardBody>
                {
                  !this.state.isReport &&
                  <Button onClick={() => this._createNew()} className="mb-2 btn btn-warning" disabled={this.state.stockcard_id === 0}>Tambah</Button>
                }
                {
                  this.state.dataSets &&
                  <CoreExcel buttonText={'Download'} datasets={this.state.dataSets}/>
                }

                <Table hover responsive className="table-outline sb-0 mb-0 d-sm-table">
                  <thead className="thead-light">
                  <tr>
                    {this._renderTableHeader()}
                  </tr>
                  </thead>
                  <tbody>
                    {this._renderTableContent()}
                    {this.state.dataList.length > 10 &&
                    <tr onClick={()=>{this.setState({loadMore : !this.state.loadMore})}}>
                      <td className='text-center' colspan={this.state.colNameList.length}>
                      {
                        this.state.dataList.length > 10 && !this.state.loadMore && <div>--Tampilkan lebih banyak--</div>
                      }
                      {
                        this.state.dataList.length > 0 && this.state.loadMore && <div>--Tampilkan lebih sedikit--</div>
                      }
                      </td>
                    </tr>
                    }
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }

  toggleConfirmDeleteModal() {
    this.setState({
      confirmDeleteModal: !this.state.confirmDeleteModal
    });
  }

  _renderConfirmDeleteModal = () =>{
    return(
      <div>
        <Modal isOpen={this.state.confirmDeleteModal} toggle={this.toggleConfirmDeleteModal} className="modal-warning modal-dialog-centered">
          <ModalBody className="text-center">
            <label className="col-form-label-lg">Apa Anda yakin untuk menghapus data?</label>
          </ModalBody>
          <ModalFooter style={flexCenter}>
            <Button color="warning" style={btn60px} onClick={()=>{
              this._deleteData(this.state.itemToDelete);
              this.setState({
                itemToDelete: {}
              });
              this.toggleConfirmDeleteModal();
            }}>Ya</Button>
            <Button color="danger" style={btn60px} onClick={this.toggleConfirmDeleteModal}>Tidak</Button>
          </ModalFooter>
        </Modal>
      </div>
    )
  };

  render() {
    return (
      <div className="animated fadeIn">
        {
          <div>
            {this._renderSearchModal()}
          </div>
        }
        {
          <div>
            {this._renderConfirmDeleteModal()}
          </div>
        }
        {
          this.state.isView &&
          <div>
            {this.renderFormTemplate()}
          </div>
        }
        {
          !this.state.isView &&
          <div>
            {this.renderListTemplate()}
          </div>
        }
      </div>
    );
  }
}

export default Base;
