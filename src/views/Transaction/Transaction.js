import React, { Component, lazy, Suspense } from 'react';
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Row,
  Table,
  Input,
  Form,
  FormGroup,
  Label,
  FormText,
  Modal, ModalBody, ModalFooter, ModalHeader
} from 'reactstrap';
import {
  Link
} from 'react-router-dom';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import Moment from 'moment';
import DatePicker from 'react-date-picker';

import Base from '../../views/Base/FormBase/Base';

import superagent from 'superagent';

import NumberFormat from 'react-number-format';
import {server} from "../../util/Constant";

const hideDisplay = {
  display: 'none'
};

class Transaction extends Base {
  constructor(props) {
    super(props);

    this.state = {
      url: server+"transaction",
      title:'Master Transaksi',
      selected_vehicle: 'Pilih Kendaraan',
      keyword:'',
      date_from: '',
      date_to: '',
      colNameList:['#','Nama Pembeli','Kode Transaksi','Tanggal Transaksi','Nama Kendaraan','Tahun Kendaraan','Plat Nomor','Nama Sales','Harga Jual','   ','   ','   '],
      dataList:[],
      vehicleList:[],
      salesList:[],
      isView:false,
      filterModal:false,
      dropdownOpen:new Array(19).fill(false),
      data:{
      }
    };
    this.toggleFilterModal = this.toggleFilterModal.bind(this);
  }

  _setInitialState(){
    this.setState({
      isView : false,
      data:{
        ... this.state.data,
        car_brand: '',
        car_color: '',
        car_name: "",
        license_plate: '',
        last_update_date: Moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
        customer_name: '',
        customer_address: '',
        total_amount: '',
        machine_no: '',
        bpkb_no: '',
        sales_name: '',
        commision: '',
        transaction_no: '',
        transaction_date: new Date(),
        status_code: "1",
        status_name: "ACTIVE",
        stnk_no: "",
        tahun: "",
        updated_by: "ADMIN",
        selected_vehicle: 'Pilih Kendaraan',
        notes1:'',
        notes2:''
      }
    })
  }

  async _getDataList(){
    this._searchData();
    await superagent
    .get(server+"stockcard/active")
    .then(res => {
      if(res.body!=null){
        this.setState({
          vehicleList : res.body
        })
      }
    });
    await superagent
    .get(server+"sales")
    .then(res => {
      if(res.body!=null){
        this.setState({
          salesList : res.body,
          data : {
            ...this.state.data,
            sales_name: res.body[0].label
          }
        })
      }
    });
    this._setInitialState();
  }

  _renderTableContent = () => {
    var that = this;
    if(this.state.loadMore){
      return this.state.dataList.map(function(data, index){
        return (
          <tr>
            <td className="text-center">
              <div>
                {index+1}
              </div>
            </td>
            <td>
              <div>{data.customer_name}</div>
            </td>
            <td>
              <div>{data.transaction_no}</div>
            </td>
            <td>
              <div>{Moment(data.transaction_date).format("DD-MM-YYYY")}</div>
            </td>
            <td>
              <div>{data.car_name}</div>
            </td>
            <td>
              <div>{data.tahun}</div>
            </td>
            <td>
              <div>{data.license_plate}</div>
            </td>
            <td>
              <div>{data.sales_name}</div>
            </td>
            <td>
              <div><NumberFormat displayType="text" value={data.total_amount} thousandSeparator={'.'} decimalSeparator={','} prefix={'Rp '} /></div>
            </td>
            <td>
            <Button onClick={()=>{
              data.transaction_date = Moment(data.transaction_date, 'YYYY-MM-DD').toDate();
              that.setState({data : data, isView:true})}}>Lihat</Button>
            </td>
            <td>
              <Button className="btn btn-warning" target="_blank" href={server + '../../pdf/document/invoice.php?id=' + data.id}>Cetak</Button>
            </td>
            <td>
              <Button className="btn btn-danger" onClick={()=>{
                that.toggleConfirmDeleteModal();
                that.setState({
                  itemToDelete: data
                })
              }}>Hapus</Button>
            </td>
          </tr>
        )
      })
    }else{
      return this.state.dataList.slice(0,9).map(function(data, index){
        return (
          <tr>
            <td className="text-center">
              <div>
                {index+1}
              </div>
            </td>
            <td>
              <div>{data.customer_name}</div>
            </td>
            <td>
              <div>{data.transaction_no}</div>
            </td>
            <td>
              <div>{Moment(data.transaction_date).format("DD-MM-YYYY")}</div>
            </td>
            <td>
              <div>{data.car_name}</div>
            </td>
            <td>
              <div>{data.tahun}</div>
            </td>
            <td>
              <div>{data.license_plate}</div>
            </td>
            <td>
              <div>{data.sales_name}</div>
            </td>
            <td>
              <div><NumberFormat displayType="text" value={data.total_amount} thousandSeparator={'.'} decimalSeparator={','} prefix={'Rp '} /></div>
            </td>
            <td>
            <Button onClick={()=>{
              data.transaction_date = Moment(data.transaction_date, 'YYYY-MM-DD').toDate();
              that.setState({data : data, isView:true})}}>Lihat</Button>
            </td>
            <td>
              <Button className="btn btn-warning" target="_blank" href={server + '../../pdf/document/invoice.php?id=' + data.id}>Cetak</Button>
            </td>
            <td>
              <Button className="btn btn-danger" onClick={()=>{
                that.toggleConfirmDeleteModal();
                that.setState({
                  itemToDelete: data
                })
              }}>Hapus</Button>
            </td>
          </tr>
        )
      })
    }

  }

  _renderSearchModalPickerData(){
    var that = this;
    return that.state.vehicleList.map(function(data, index){
      if (data.status_code !== 2) {
        return (
          <tr onClick={() => {
            {
              that.setState(prevState => ({
                stockcard_id: data.id,
                selected_vehicle: data.car_name + ' - ' + data.license_plate,
                selected_vehicle_price: data.purchase_price,
                searchModal : false,
                data: {
                  ...prevState.data,
                  stockcard_id: data.id,
                  car_name: data.car_name,
                  car_brand: data.car_brand,
                  tahun: data.tahun,
                  car_color: data.car_color,
                  license_plate: data.license_plate,
                  stnk_no: data.stnk_no
                }
              }));
            }
          }}
          >
            <td className="text-left">
              <div>
                {index + 1}
              </div>
            </td>
            <td className="text-center">
              <div>{data.license_plate}</div>
            </td>
            <td className="text-center">
              <div>{data.car_name}</div>
            </td>
            <td className="text-center">
              <div>{data.car_color}</div>
            </td>
            <td className="text-center">
              <div>{data.tahun}</div>
            </td>
          </tr>
        )
      }
    })
  }

  _renderSearchModal = () =>{
    return(
      <div>
        <Modal isOpen={this.state.searchModal} toggle={this.toggleSearchModal} className="modal-warning">
          <ModalHeader toggle={this.toggleSearchModal}>Pilih Kendaraan</ModalHeader>
          <ModalBody>
            <div className="table-wrapper-scroll-y my-custom-scrollbar">
              <Table hover responsive="sm" className="table-outline sb-0 mb-0 d-none d-sm-table">
                <tbody>
                {this._renderSearchModalPickerData()}
                </tbody>
              </Table>
            </div>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleSearchModal}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    )
  }

  toggleFilterModal() {
    this.state.filterModal && this._searchData();
    this.setState({
      filterModal: !this.state.filterModal,
    });
  }

  _renderFilterModal = () =>{
    return(
      <div>
        <Modal isOpen={this.state.filterModal} toggle={this.toggleFilterModal} className="modal-warning">
          <ModalHeader toggle={this.toggleFilterModal}>Filter Tanggal</ModalHeader>
          <ModalBody>
            <Row>
            <Col xs={6}>
              <Label>Date From:</Label><br/>
              <DatePicker
                format={'dd-MM-yyyy'}
                locale={'id'}
                onChange={(e)=>{
                  this.setState({
                    date_from: e !== null ? e : ''
                  })
                }}
                value={this.state.date_from}
              />
            </Col>
            <Col xs={6}>
              <Label>Date To:</Label><br/>
              <DatePicker
                format={'dd-MM-yyyy'}
                locale={'id'}
                onChange={(e)=>{
                  this.setState({
                    date_to: e !== null ? e : ''
                  })
                }}
                value={this.state.date_to}
              />
            </Col>
            </Row>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleFilterModal}>Submit</Button>
          </ModalFooter>
        </Modal>
      </div>
    )
  };

  renderHeaderList() {
    return (
      <div>
        <Row>
          <Col sm={6} xs={12}>
            <h3>{this.state.title}</h3>
          </Col>
          <Col sm={6} xs={12} className="text-right d-inline-flex">
            <Button className="btn btn-success" onClick={() => {
              this.toggleFilterModal();
            }}>Filter</Button>
            <Input value={this.state.keyword} onChange={(e)=>{this.setState({
              keyword:e.target.value
            })}} className="mx-2"></Input>
            <Button className="btn btn-success" onClick={this._searchData}>Search</Button>
          </Col>
        </Row>
      </div>
    )
  }

  renderForm(){
    return(
      <Form>
        <FormGroup>
          <Label for="date">Tanggal Transaksi</Label>
          <br/>
          <DatePicker
            format={'dd-MM-yyyy'}
            locale={'id'}
            onChange={(e)=>{
              this.setState({
                data : {
                  ... this.state.data,
                  transaction_date: e
                }
              })
            }}
            value={this.state.data.transaction_date}
          />
        </FormGroup>
        <FormGroup>
          <Label for="buyerName">Nama Pembeli</Label>
          <Input disabled={this.state.data.status_code!=1} type="text" value={this.state.data.customer_name || ''} name="buyerName" id="inputBuyerName" onChange={(e)=>{
            this.setState({
              data:{
                ...this.state.data,
                customer_name : e.target.value
              }
            })
          }} >
          </Input>
        </FormGroup>
        <FormGroup>
          <Label for="buyerAddress">Alamat</Label>
          <Input disabled={this.state.data.status_code!=1} type="text" value={this.state.data.customer_address} name="buyerAddress" id="inputBuyerAddress" onChange={(e)=>{
            this.setState({
              data:{
                ...this.state.data,
                customer_address : e.target.value
              }
            })
          }} >
          </Input>
        </FormGroup>
        <FormGroup>
          <Row>
            <Col className="text-left pull-left" md={12} xs={12}>
              <Label className="text-left" for="totalAmount">Jumlah Pembayaran</Label>
            </Col>
            <Col className="text-left pull-left" md={12} xs={12}>
              <NumberFormat disabled={this.state.data.status_code!=1} style={{width:'100%'}} value={this.state.data.total_amount} thousandSeparator={'.'} decimalSeparator={','}
                            prefix={'Rp '} onValueChange={(values) => {
                const {formattedValue, value} = values;
                // formattedValue = $2,223
                // value ie, 2223
                this.setState({
                  data :{
                    ... this.state.data,
                    total_amount : value
                  }
                })
              }}/>
            </Col>
          </Row>
        </FormGroup>
        <FormGroup>
          <Button className="btn btn-success" onClick={()=>{
            this.toggleSearchModal();
          }}>{this.state.selected_vehicle}</Button>
        </FormGroup>
        <FormGroup>
          <Label for="brand">Merk</Label>
          <Input disabled type="text" value={this.state.data.car_brand} name="brand" id="inputBrand" />
        </FormGroup>
        <FormGroup>
          <Label for="tahun">Tahun Kendaraan</Label>
          <Input disabled type="text" value={this.state.data.tahun} name="year" id="inputYear" />
        </FormGroup>
        <FormGroup>
          <Label for="color">Warna</Label>
          <Input disabled type="text" value={this.state.data.car_color}  name="color" id="inputColor" />
        </FormGroup>
        <FormGroup>
          <Label for="frameNo">Nomor Rangka</Label>
          <Input disabled type="text" value={this.state.data.stnk_no}  name="color" id="inputFrameNo" />
        </FormGroup>
        <FormGroup>
          <Label for="machineNo">Nomor Mesin</Label>
          <Input disabled={this.state.data.status_code!=1} type="text" value={this.state.data.machine_no || ''} name="machineNo" id="inputMachineNo" onChange={(e)=>{
            this.setState({
              data:{
                ...this.state.data,
                machine_no : e.target.value
              }
            })
          }} >
          </Input>
        </FormGroup>
        <FormGroup style={hideDisplay}>
          <Label for="bpkbNo">Nomor BPKB</Label>
          <Input disabled={this.state.data.status_code!=1} type="text" value={this.state.data.bpkb_no || ''} name="bpkbNo" id="inputBpkbNo" onChange={(e)=>{
            this.setState({
              data:{
                ...this.state.data,
                bpkb_no : e.target.value
              }
            })
          }} >
          </Input>
        </FormGroup>
        <FormGroup>
          <Label for="salesName">Nama Sales</Label>
          <Input disabled={this.state.data.status_code!=1} type="select" value={this.state.data.sales_name}  name="salesName" id="inputSalesName" onChange={(e)=>{
            this.setState({
              data:{
                ...this.state.data,
                sales_name : e.target.value
              }
            })
          }} >
            {this.renderOption(this.state.salesList)}
          </Input>
        </FormGroup>
        <FormGroup>
          <Row>
            <Col className="text-left pull-left" md={12} xs={12}>
              <Label className="text-left" for="commision">Komisi</Label>
            </Col>
            <Col className="text-left pull-left" md={12} xs={12}>
              <NumberFormat disabled={this.state.data.status_code!=1} style={{width:'100%'}} value={this.state.data.commision} thousandSeparator={'.'} decimalSeparator={','}
                            prefix={'Rp '} onValueChange={(values) => {
                const {formattedValue, value} = values;
                // formattedValue = $2,223
                // value ie, 2223
                this.setState({
                  data :{
                    ... this.state.data,
                    commision : value
                  }
                })
              }}/>
            </Col>
          </Row>
        </FormGroup>
        <FormGroup>
          <Label for="notes1">Keterangan</Label>
          <Input disabled={this.state.data.status_code!=1} value={this.state.data.notes1} name="notes1" id="inputNotes1" onChange={(e)=>{
            this.setState({
              data:{
                ...this.state.data,
                notes1 : e.target.value
              }
            })
          }} />
          <br/>
          <Input disabled={this.state.data.status_code!=1} value={this.state.data.notes2} name="notes2" id="inputNotes2" onChange={(e)=>{
            this.setState({
              data:{
                ...this.state.data,
                notes2 : e.target.value
              }
            })
          }} />
        </FormGroup>
        <Row className="pull-right">
          <Col >
            <Button disabled={this.state.data.status_code!=1} className="pull-right btn btn-success" onClick={()=>this._saveData()}>Simpan</Button>
          </Col>
        </Row>
      </Form>
    )
  }

  render() {
    return (
      <div className="animated fadeIn">
        {
          <div>
            {this._renderSearchModal()}
          </div>
        }
        {
          <div>
            {this._renderConfirmDeleteModal()}
          </div>
        }
        {
          <div>
            {this._renderFilterModal()}
          </div>
        }
        {
          this.state.isView &&
          <div>
            {this.renderFormTemplate()}
          </div>
        }
        {
          !this.state.isView &&
          <div>
            {this.renderListTemplate()}
          </div>
        }
      </div>
    );
  }
}

export default Transaction;
