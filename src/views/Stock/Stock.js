import React, { Component, lazy, Suspense } from 'react';
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Row,
  Table,
  Input,
  Form,
  FormGroup,
  Label,
  FormText
} from 'reactstrap';
import {
  Link
} from 'react-router-dom';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import Moment from 'moment';
import DatePicker from 'react-date-picker';

import Base from '../../views/Base/FormBase/Base';

import superagent from 'superagent';

import NumberFormat from 'react-number-format';

import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

import { FilePond, registerPlugin } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import FilePondPluginImageExifOrientation from 'filepond-plugin-image-exif-orientation';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';
import FilePondPluginFileEncode from 'filepond-plugin-file-encode';
registerPlugin(FilePondPluginImageExifOrientation, FilePondPluginImagePreview, FilePondPluginFileEncode);

class Stock extends Base {
  constructor(props) {
    super(props);

    this.state = {
      url:"https://dianmobil.com/dealer/index.php/stockcard",
      title:'Master Kendaraan',
      keyword:'',
      colNameList:['#','Merk Kendaraan','Nama Kendaraan','Tahun Kendaraan','Plat Nomor','Warna','Harga Beli','Status','   ','   ','   '],
      dataList:[],
      brandList:[],
      colorList:[],
      typeList:[],
      fuelList:[],
      transmisionList:[],
      purchaserList:[],
      isView:false,
      isEdit:false,
      files:[],
      dropdownOpen:new Array(19).fill(false),
      data:{
      }
    };
  }

  handleInit() {
    console.log("FilePond instance has initialised", this.pond);
  }

  _setInitialState(){
    this.setState({
      files: [],
      isView: false,
      isEdit: false,
      data:{
        ... this.state.data,
        car_brand: this.state.brandList[0].label,
        car_color: this.state.colorList[0].label,
        car_fuel: this.state.fuelList[0].label,
        car_name: "",
        car_transmision: this.state.transmisionList[0].label,
        car_type: this.state.typeList[0].label,
        car_images: [],
        last_update_date: Moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
        license_plate: "",
        purchase_date: new Date(),
        purchase_price: "",
        purchaser_name: this.state.purchaserList[0].label,
        status_code: "1",
        status_name: "ACTIVE",
        stnk_no: "",
        tahun: "",
        updated_by: "ADMIN"
      }
    })
  }

  async _getDataList(){
    this._searchData();
    await superagent
    .get("https://dianmobil.com/dealer/index.php/systable?syscode=BND")
    .then(res => {
      if(res.body!=null){
        this.setState({
          brandList : res.body,
          data : {
            ... this.state.data,
            car_brand : res.body[0].label
          }
        })
      }
    });
    await superagent
    .get("https://dianmobil.com/dealer/index.php/systable?syscode=CLR")
    .then(res => {
      if(res.body!=null){
        this.setState({
          colorList : res.body,
          data : {
            ... this.state.data,
            car_color : res.body[0].label
          }
        })
      }
    });
    await superagent
    .get("https://dianmobil.com/dealer/index.php/systable?syscode=TYP")
    .then(res => {
      if(res.body!=null){
        this.setState({
          typeList : res.body,
          data : {
            ... this.state.data,
            car_type : res.body[0].label
          }

        })
      }
    });
    await superagent
    .get("https://dianmobil.com/dealer/index.php/systable?syscode=BBM")
    .then(res => {
      if(res.body!=null){
        this.setState({
          fuelList : res.body,
          data : {
            ... this.state.data,
            car_fuel : res.body[0].label
          }
        })
      }
    });
    await superagent
    .get("https://dianmobil.com/dealer/index.php/systable?syscode=TMS")
    .then(res => {
      if(res.body!=null){
        this.setState({
          transmisionList : res.body,
          data : {
            ... this.state.data,
            car_transmision : res.body[0].label
          }
        })
      }
    });
    await superagent
    .get("https://dianmobil.com/dealer/index.php/purchaser")
    .then(res => {
      if(res.body!=null){
        this.setState({
          purchaserList : res.body,
          data : {
            ... this.state.data,
            purchaser_name : res.body[0].label
          }
        })
      }
    });
    this._setInitialState();
  }

  _sell(data){
      var that = this;
      data.status_code= 2;
      data.status_name = 'SOLD';
      superagent.put(this.state.url)
      .set('Content-Type', 'application/json')
      .send(data)
      .end(function(err,res){
        if(res.statusCode==200){
          window.alert("Berhasil Menjual Mobil");
          that.setState({
            data:{
              bulan :1,
              tahun : 2019
            },
            isView:false
          });
          that._getDataList()
        }
      });

  }

  _cancelSell(data){
      var that = this;
      data.status_code= 1;
      data.status_name = 'ACTIVE';
      superagent.put(this.state.url)
      .set('Content-Type', 'application/json')
      .send(data)
      .end(function(err,res){
        if(res.statusCode==200){
          window.alert("Berhasil Membatalkan Penjualan Mobil");
          that.setState({
            data:{
              bulan :1,
              tahun : 2019
            },
            isView:false
          });
          that._getDataList()
        }
      });

  }

  Slideshow = () => {
    return (
      <div style={{maxWidth: 554}}>
        <Carousel>
          <div>
            <img src={`https://dianmobil.com/dealer/uploads/images/${this.state.data.car_images_1 || `../image-not-found.png`}`} />
          </div>
          <div>
            <img src={`https://dianmobil.com/dealer/uploads/images/${this.state.data.car_images_2 || `../image-not-found.png`}`} />
          </div>
          <div>
            <img src={`https://dianmobil.com/dealer/uploads/images/${this.state.data.car_images_3 || `../image-not-found.png`}`} />
          </div>
          <div>
            <img src={`https://dianmobil.com/dealer/uploads/images/${this.state.data.car_images_4 || `../image-not-found.png`}`} />
          </div>
          <div>
            <img src={`https://dianmobil.com/dealer/uploads/images/${this.state.data.car_images_5 || `../image-not-found.png`}`} />
          </div>
        </Carousel>
      </div>
    )
  }

  _renderTableContent = () => {
    var that = this;
    if(this.state.loadMore){
      return this.state.dataList.map(function(data, index){
        return (
          <tr>
            <td className="text-center">
              <div>
                {index+1}
              </div>
            </td>
            <td>
              <div>{data.car_brand}</div>
            </td>
            <td className="text-center">
              <div>{data.car_name}</div>
            </td>
            <td className="text-center">
              <div>{data.tahun}</div>
            </td>
            <td>
              <div>{data.license_plate}</div>
            </td>
            <td>
              <div>{data.car_color}</div>
            </td>
            <td className="text-center">
              <div><NumberFormat displayType="text" type="text" style={{width:'100%'}} value={data.purchase_price} thousandSeparator={'.'} decimalSeparator={','}
                                 prefix={'Rp '} onValueChange={(values) => {
                const {formattedValue, value} = values;
                // formattedValue = $2,223
                // value ie, 2223
                this.setState({
                  data :{
                    ...this.state.data,
                    purchase_price : value
                  }
                })
              }}/></div>
            </td>
            <td className="text-center">
              {
                data.status_name === 'SOLD' && <div style={{ color: 'green', fontWeight: 'bold'}}>{data.status_name}</div>
              }
              {
                data.status_name !== 'SOLD' && <div style={{ color: '#000'}}>{data.status_name}</div>
              }
            </td>
            <td>
              <img src={`https://dianmobil.com/dealer/uploads/images/${data.car_images_1 || `../image-not-found.png`}`} style={{width:48,height:36}} />
            </td>
            <td>
            <Button onClick={()=>{
              data.purchase_date = Moment(data.purchase_date, 'YYYY-MM-DD').toDate()
              that.setState({data: data, isView: true, isEdit: true})}}>Lihat</Button>
            </td>
            <td>
              <Button disabled={data.status_code!=1} className="btn btn-danger" onClick={()=>{
                that.toggleConfirmDeleteModal();
                that.setState({
                  itemToDelete: data
                })
              }}>Hapus</Button>
            </td>
          </tr>
        )
      })
    }else{
      return this.state.dataList.slice(0,9).map(function(data, index){
        return (
          <tr>
            <td className="text-center">
              <div>
                {index+1}
              </div>
            </td>
            <td>
              <div>{data.car_brand}</div>
            </td>
            <td className="text-center">
              <div>{data.car_name}</div>
            </td>
            <td className="text-center">
              <div>{data.tahun}</div>
            </td>
            <td>
              <div>{data.license_plate}</div>
            </td>
            <td>
              <div>{data.car_color}</div>
            </td>
            <td className="text-center">
              <div><NumberFormat displayType="text" type="text" style={{width:'100%'}} value={data.purchase_price} thousandSeparator={'.'} decimalSeparator={','}
                prefix={'Rp '} onValueChange={(values) => {
                const {formattedValue, value} = values;
                // formattedValue = $2,223
                // value ie, 2223
                this.setState({
                  data :{
                    ...this.state.data,
                    purchase_price : value
                  }
                })
              }}/></div>
            </td>
            <td className="text-center">
              {
                data.status_name === 'SOLD' && <div style={{ color: 'green', fontWeight: 'bold'}}>{data.status_name}</div>
              }
              {
                data.status_name !== 'SOLD' && <div style={{ color: '#000'}}>{data.status_name}</div>
              }
            </td>
            <td>
              <img src={`https://dianmobil.com/dealer/uploads/images/${data.car_images_1 || `../image-not-found.png`}`} style={{width:48,height:36}} />
            </td>
            <td>
            <Button onClick={()=>{
              data.purchase_date = Moment(data.purchase_date, 'YYYY-MM-DD').toDate()
              that.setState({data: data, isView: true, isEdit: true})}}>Lihat</Button>
            </td>
            <td>
              <Button disabled={data.status_code!=1} className="btn btn-danger" onClick={()=>{
                that.toggleConfirmDeleteModal();
                that.setState({
                  itemToDelete: data
                })
              }}>Hapus</Button>
            </td>
          </tr>
        )
      })
    }

  }

  renderForm(){
    return(
      <Form>
        <FormGroup>
          <Label for="licensePlate">Nomor Polisi</Label>
          <Input type="text" value={this.state.data.license_plate || ''} name="licensePlate" id="inputLicensePlate" onChange={(e)=>{
            this.setState({
              data:{
                ...this.state.data,
                license_plate : e.target.value
              }
            })
          }} >
          </Input>
        </FormGroup>
        <FormGroup>
          <Label for="stnkNo">Nomor Rangka</Label>
          <Input type="text" value={this.state.data.stnk_no} name="stnkNo" id="inputStnkNo" onChange={(e)=>{
            this.setState({
              data:{
                ...this.state.data,
                stnk_no : e.target.value
              }
            })
          }} >
          </Input>
        </FormGroup>
        <FormGroup>
          <Label for="name">Nama Kendaraan</Label>
          <Input type="text" value={this.state.data.car_name} name="name" id="inputName" onChange={(e)=>{
            this.setState({
              data:{
                ...this.state.data,
                car_name : e.target.value
              }
            })
          }} >
          </Input>
        </FormGroup>
        <FormGroup>
          <Label for="brand">Merk</Label>
          <Input type="select" value={this.state.data.car_brand} name="brand" id="inputBrand" onChange={(e)=>{
            this.setState({
              data:{
                ...this.state.data,
                car_brand : e.target.value
              }
            })
          }} >
            {this.renderOption(this.state.brandList)}
          </Input>
        </FormGroup>
        <FormGroup>
          <Label for="color">Warna</Label>
          <Input type="select" value={this.state.data.car_color}  name="color" id="inputColor" onChange={(e)=>{
            this.setState({
              data:{
                ...this.state.data,
                car_color : e.target.value
              }
            })
          }} >
            {this.renderOption(this.state.colorList)}
          </Input>
        </FormGroup>
        <FormGroup>
          <Label for="transmision">Transmisi</Label>
          <Input type="select" value={this.state.data.car_transmision}  name="transmision" id="inputTransmision" onChange={(e)=>{
            this.setState({
              data:{
                ...this.state.data,
                car_transmision : e.target.value
              }
            })
          }} >
            {this.renderOption(this.state.transmisionList)}
          </Input>
        </FormGroup>
        <FormGroup>
          <Label for="fuel">Bahan Bakar</Label>
          <Input type="select" value={this.state.data.car_fuel}  name="fuel" id="inputFuel" onChange={(e)=>{
            this.setState({
              data:{
                ...this.state.data,
                car_fuel : e.target.value
              }
            })
          }} >
            {this.renderOption(this.state.fuelList)}
          </Input>
        </FormGroup>
        <FormGroup>
          <Label for="fuel">Tipe Bodi</Label>
          <Input type="select" value={this.state.data.car_type}  name="type" id="inputType" onChange={(e)=>{
            this.setState({
              data:{
                ...this.state.data,
                car_type : e.target.value
              }
            })
          }} >
            {this.renderOption(this.state.typeList)}
          </Input>
        </FormGroup>
        <FormGroup>
          <Label for="date">Tanggal Pembelian</Label>
          <br/>
          <DatePicker

            format={'dd-MM-yyyy'}
            locale={'id'}
            onChange={(e)=>{
              this.setState({
                data : {
                  ... this.state.data,
                  purchase_date: e
                }
              })
            }}
            value={this.state.data.purchase_date}
          />
        </FormGroup>
        <FormGroup>
          <Label for="tahun">Tahun Kendaraan</Label>
          <Input

            type="number"
            name="year"
            id="inputYear"
            placeholder="Tahun"
            value={this.state.data.tahun}
            onChange={(e)=>{
              this.setState({
                data :{
                  ... this.state.data,
                  tahun : e.target.value
                }
              })
            }}
          />
        </FormGroup>
        <FormGroup>
          <Label for="purchaser">Purchaser</Label>
          <Input type="select" value={this.state.data.purchaser_name} name="purchaserName" id="inputPurchaserName" onChange={(e)=>{
            this.setState({
              data:{
                ...this.state.data,
                purchaser_name : e.target.value
              }
            })
          }} >
            {this.renderOption(this.state.purchaserList)}
          </Input>
        </FormGroup>
        <FormGroup>
          <Row>
            <Col className="text-left pull-left" md={12} xs={12}>
              <Label className="text-left" for="purchasePrice">Harga Beli</Label>
            </Col>
            <Col className="text-left pull-left" md={12} xs={12}>
              <NumberFormat disabled={this.state.data.status_code!=1} style={{width:'100%'}} value={this.state.data.purchase_price} thousandSeparator={'.'} decimalSeparator={','}
                prefix={'Rp '} onValueChange={(values) => {
                const {formattedValue, value} = values;
                // formattedValue = $2,223
                // value ie, 2223
                this.setState({
                  data :{
                    ... this.state.data,
                    purchase_price : value
                  }
                })
              }}/>
            </Col>
          </Row>
        </FormGroup>
        <FormGroup>
          {
            this.state.isEdit && this.Slideshow()
          }
          <Label>Unggah Gambar</Label>
          <FilePond
            ref={ref => this.pond = ref}
            files={this.state.files}
            allowMultiple={true}
            maxFiles={5}
            imageResizeTargetWidth="200"
            imageResizeTargetHeight="200"
            server={{
              // fake server to simulate loading a 'local' server file and processing a file
              process: (fieldName, file, metadata, load) => {
                // simulates uploading a file
                setTimeout(() => {
                  load(Date.now());
                }, 1500);
              },
              load: (source, load) => {
                // simulates loading a file from the server
                fetch(source)
                .then(res => {
                  return res.blob();
                })
                .then(blob => {
                  return load(blob);
                });
              }
            }}
            oninit={() => this.handleInit()}
            onupdatefiles={fileItems => {
              // Set currently active file objects to this.state
              this.setState({
                files: fileItems.map(fileItem => fileItem.file),
                data: {
                  ...this.state.data,
                  car_images : fileItems.map(fileItem => fileItem.getFileEncodeBase64String())
                }
              });
            }}
          />
        </FormGroup>
        <Row className="pull-right">
          <Col >
            <Button className="pull-right btn btn-success" onClick={()=>this._saveData()} disabled={this.state.isEdit === false && this.state.data.car_images.length === 0}>Simpan</Button>
          </Col>
        </Row>
      </Form>
    )
  }
}

export default Stock;
