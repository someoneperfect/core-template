import React, { Component, lazy, Suspense } from 'react';
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Row,
  Table,
  Input,
  Form,
  FormGroup,
  Label,
  FormText,
  Modal, ModalBody, ModalFooter, ModalHeader
} from 'reactstrap';
import {
  Link
} from 'react-router-dom';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import Moment from 'moment';
import DatePicker from 'react-date-picker';

import Base from '../../views/Base/FormBase/Base';

import superagent from 'superagent';

import NumberFormat from 'react-number-format';
import {server} from "../../util/Constant";

class Report extends Base {
  constructor(props) {
    super(props);
    const dateFrom = new Date();
    this.state = {
      url: server+"report/labarugi",
      title:'Report',
      keyword:'',
      date_from: new Date(dateFrom.setDate(dateFrom.getDate() - 30)),
      date_to: new Date(),
      colNameList:['#','Tanggal Transaksi','Plat Nomor','Nama Kendaraan','Warna','Tahun Kendaraan','Nama Sales','Harga Jual','HPP','Komisi','Laba/Rugi'],
      dataList:[],
      isView:false,
      isReport:true,
      filterModal:false,
      dropdownOpen:new Array(19).fill(false),
      summary : {
        total_amount : 0,
        hpp : 0,
        komisi : 0,
        labarugi : 0
      }
    };
    this.toggleFilterModal = this.toggleFilterModal.bind(this);
  }

  _setInitialState(){
    this.setState({
      isView : false,
      data:{
        ...this.state.data,
        car_brand: '',
        car_color: '',
        car_name: "",
        license_plate: '',
        last_update_date: Moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
        customer_name: '',
        customer_address: '',
        total_amount: '',
        machine_no: '',
        bpkb_no: '',
        sales_name: '',
        commision: '',
        transaction_no: '',
        transaction_date: new Date(),
        status_code: "1",
        status_name: "ACTIVE",
        stnk_no: "",
        tahun: "",
        updated_by: "ADMIN",
        hpp:'',
        labarugi:''
      }
    })
  }

  async _getDataList(){
    this._searchData();
    this._setInitialState();
  }

  _renderTableContent = () => {
    var that = this;
    if(this.state.loadMore){
      return this.state.dataList.map(function(data, index){
        return (
          <tr>
            <td className="text-center">
              <div>
                {index+1}
              </div>
            </td>
            <td>
              <div>{Moment(data.transaction_date).format("DD-MM-YYYY")}</div>
            </td>
            <td>
              <div>{data.license_plate}</div>
            </td>
            <td>
              <div>{data.car_name}</div>
            </td>
            <td>
              <div>{data.car_color}</div>
            </td>
            <td>
              <div>{data.tahun}</div>
            </td>
            <td>
              <div>{data.sales_name}</div>
            </td>
            <td>
              <div><NumberFormat displayType="text" value={data.total_amount} thousandSeparator={'.'} decimalSeparator={','} prefix={'Rp '} /></div>
            </td>
            <td>
              <div><NumberFormat displayType="text" value={data.hpp} thousandSeparator={'.'} decimalSeparator={','} prefix={'Rp '} /></div>
            </td>
            <td>
              <div><NumberFormat displayType="text" value={data.commision} thousandSeparator={'.'} decimalSeparator={','} prefix={'Rp '} /></div>
            </td>
            <td>
              <div><NumberFormat displayType="text" value={data.labarugi} thousandSeparator={'.'} decimalSeparator={','} prefix={'Rp '} /></div>
            </td>
          </tr>
        )
      })
    }else{
      return this.state.dataList.slice(0,9).map(function(data, index){
        return (
          <tr>
            <td className="text-center">
              <div>
                {index+1}
              </div>
            </td>
            <td>
              <div>{Moment(data.transaction_date).format("DD-MM-YYYY")}</div>
            </td>
            <td>
              <div>{data.license_plate}</div>
            </td>
            <td>
              <div>{data.car_name}</div>
            </td>
            <td>
              <div>{data.car_color}</div>
            </td>
            <td>
              <div>{data.tahun}</div>
            </td>
            <td>
              <div>{data.sales_name}</div>
            </td>
            <td>
              <div><NumberFormat displayType="text" value={data.total_amount} thousandSeparator={'.'} decimalSeparator={','} prefix={'Rp '} /></div>
            </td>
            <td>
              <div><NumberFormat displayType="text" value={data.hpp} thousandSeparator={'.'} decimalSeparator={','} prefix={'Rp '} /></div>
            </td>
            <td>
              <div><NumberFormat displayType="text" value={data.commision} thousandSeparator={'.'} decimalSeparator={','} prefix={'Rp '} /></div>
            </td>
            <td>
              <div><NumberFormat displayType="text" value={data.labarugi} thousandSeparator={'.'} decimalSeparator={','} prefix={'Rp '} /></div>
            </td>
          </tr>
        )
      })
    }

  }

  toggleFilterModal() {
    this.state.filterModal && this._searchData();
    this.setState({
      filterModal: !this.state.filterModal,
    });
  }

  _renderFilterModal = () =>{
    return(
      <div>
        <Modal isOpen={this.state.filterModal} toggle={this.toggleFilterModal} className="modal-warning">
          <ModalHeader toggle={this.toggleFilterModal}>Filter Tanggal</ModalHeader>
          <ModalBody>
            <Row>
            <Col xs={6}>
              <Label>Date From:</Label><br/>
              <DatePicker
                format={'dd-MM-yyyy'}
                locale={'id'}
                onChange={(e)=>{
                  this.setState({
                    date_from: e !== null ? e : ''
                  })
                }}
                value={this.state.date_from}
              />
            </Col>
            <Col xs={6}>
              <Label>Date To:</Label><br/>
              <DatePicker
                format={'dd-MM-yyyy'}
                locale={'id'}
                onChange={(e)=>{
                  this.setState({
                    date_to: e !== null ? e : ''
                  })
                }}
                value={this.state.date_to}
              />
            </Col>
            </Row>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleFilterModal}>Submit</Button>
          </ModalFooter>
        </Modal>
      </div>
    )
  };

  renderHeaderList() {
    return (
      <div>
        <Row className="mb-3">
          <Col sm={6} xs={12}>
            <h3>{this.state.title}</h3>
          </Col>
          <Col sm={6} xs={12} className="text-right d-inline-flex">
            <Button className="btn btn-success" onClick={() => {
              this.toggleFilterModal();
            }}>Filter</Button>
            <Input value={this.state.keyword} onChange={(e)=>{this.setState({
              keyword:e.target.value
            })}} className="mx-2"></Input>
            <Button className="btn btn-success" onClick={this._searchData}>Search</Button>
          </Col>
        </Row>
        <Row>
          <Col md={3} sm={6} xs={12}>
            <h5>Total Penjualan : <span style={{display: 'inline-block'}}><NumberFormat displayType={'text'} prefix={'Rp '} thousandSeparator={'.'} decimalSeparator={','} value={this.state.summary.total_amount} /></span></h5>
          </Col>
          <Col md={3} sm={6} xs={12}>
            <h5>Total HPP : <span style={{display: 'inline-block'}}><NumberFormat displayType={'text'} prefix={'Rp '} thousandSeparator={'.'} decimalSeparator={','} value={this.state.summary.hpp} /></span></h5>
          </Col>
          <Col md={3} sm={6} xs={12}>
            <h5>Total Komisi : <span style={{display: 'inline-block'}}><NumberFormat displayType={'text'} prefix={'Rp '} thousandSeparator={'.'} decimalSeparator={','} value={this.state.summary.komisi} /></span></h5>
          </Col>
          <Col md={3} sm={6} xs={12}>
            <h5>Total Laba/Rugi : <span style={{display: 'inline-block'}}><NumberFormat displayType={'text'} prefix={'Rp '} thousandSeparator={'.'} decimalSeparator={','} value={this.state.summary.labarugi} /></span></h5>
          </Col>
        </Row>
      </div>
    )
  }

  render() {
    return (
      <div className="animated fadeIn">
        {
          <div>
            {this._renderFilterModal()}
          </div>
        }
        {
          this.state.isView &&
          <div>
            {this.renderFormTemplate()}
          </div>
        }
        {
          !this.state.isView &&
          <div>
            {this.renderListTemplate()}
          </div>
        }
      </div>
    );
  }
}

export default Report;
