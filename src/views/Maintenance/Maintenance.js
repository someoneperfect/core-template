import React, { Component, lazy, Suspense } from 'react';
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Row,
  Table,
  Input,
  Form,
  FormGroup,
  Label,
  FormText,
  Modal, ModalBody, ModalFooter, ModalHeader
} from 'reactstrap';
import {
  Link
} from 'react-router-dom';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import Moment from 'moment';
import DatePicker from 'react-date-picker';
import NumberFormat from 'react-number-format';

import Base from '../../views/Base/FormBase/Base';
import {server} from '../../util/Constant';

import superagent from 'superagent';
import styled from 'styled-components';

const LeftAlignContainer = styled.div`
  @media only screen and (max-width: 575px) {
    text-align: left !important;
  }
`;

class Maintenance extends Base {
  constructor(props) {
    super(props);

    this.state = {
      url:server+"maintenance",
      title:'Transaksi Maintenance Kendaraan',
      stockcard_id:0,
      selected_vehicle:'Pilih Plat Nomor',
      selected_vehicle_price:0,
      total_maintenance :0,
      colNameList:['#','Tanggal','Jenis Maintenance','Biaya','Nama Operator','   ','   '],
      dataList:[],
      vehicleList:[],
      maintenanceList:[],
      loadMore:false,
      isView:false,
      sold:false,
      dropdownOpen:new Array(19).fill(false),
      data:{
      }
    };
  }

  _setInitialState(){
    this.setState({
      isView : false,
      data:{
        ...this.state.data,
        maintenance_cost:0,
        maintenance_name:'',
        maintenance_date: new Date(),
        description1:'',
        description2:'',
        stockcard_id:0,
        operator_name:'',
        status_code:1,
        loadMore:false,
        selected_vehicle:'Pilih Plat Nomor',
        selected_vehicle_price:0,
        sold:false,
        status_name:'ACTIVE',
        updated_by: "ADMIN",
        last_update_date: Moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
      }
    })

  }

  _createNew(){
      this._setInitialState();
      this.setState({
        isView:true,
        data : {
          ...this.state.data,
          stockcard_id : this.state.stockcard_id
        }
      });
  }

  renderHeaderList(){
    return(
      <div>
        <Row className='mb-3'>
          <Col sm={6} xs={12}>
            <h3>{this.state.title}</h3>
          </Col>
          <Col className='pull-right text-right' md={6} xs={12}>
            <LeftAlignContainer>
                <Button className="btn btn-success" onClick={()=>{
                  this.toggleSearchModal();
                }}>{this.state.selected_vehicle}</Button>
            </LeftAlignContainer>
          </Col>
        </Row>
        <Row>
          <Col sm={4} xs={12}>
            <h5>Harga Mobil : <span><NumberFormat displayType={'text'} prefix={'Rp '} thousandSeparator={'.'} decimalSeparator={','} value={this.state.selected_vehicle_price}/></span></h5>
          </Col>
          <Col sm={4} xs={12}>
            <h5>Total Maintenance : <span><NumberFormat displayType={'text'} prefix={'Rp '} thousandSeparator={'.'} decimalSeparator={','} value={this.state.total_maintenance}/></span></h5>
          </Col>
          <Col className='pull-right text-right' sm={4} xs={12}>
            <LeftAlignContainer>
              <h5>HPP : <span><NumberFormat displayType={'text'} prefix={'Rp '} thousandSeparator={'.'} decimalSeparator={','} value={parseInt(this.state.total_maintenance)+parseInt(this.state.selected_vehicle_price)}/></span></h5>
            </LeftAlignContainer>
          </Col>
        </Row>
        {
          this.state.sold &&
          <Row>
            <Col xs={12}>
              <h3 className="text-center">SOLD</h3>
            </Col>
          </Row>
        }
      </div>
    )
  }

  async _getDataList(){
    this._setInitialState();
    await superagent
    .get(server+"systable?syscode=MNT")
    .then(res => {
      if(res.body!=null){
        this.setState({
          maintenanceList : res.body,
          data : {
            ... this.state.data,
            maintenance_name : res.body[0].label
          }
        })
      }
    });
    await superagent
    .get(server+"stockcard")
    .then(res => {
      if(res.body!=null){
        this.setState({
          vehicleList : res.body
        })
      }
    });
  }

  _renderTableContent = () => {
    var that = this;

    if(that.state.loadMore){
      return this.state.dataList.map(function(data, index){
        return (
          <tr>
            <td className="text-center">
              <div>
                {index+1}
              </div>
            </td>
            <td>
              <div>{Moment(data.maintenance_date).format("DD-MM-YYYY")}</div>
            </td>
            <td className="text-center">
              <div>{data.maintenance_name}</div>
            </td>
            <td>
              <div><NumberFormat value={data.maintenance_cost}/></div>
            </td>
            <td className="text-center">
              <div>{data.operator_name}</div>
            </td>
            <td>
            <Button onClick={()=>{
              data.maintenance_date = Moment(data.maintenance_date, 'YYYY-MM-DD').toDate()
              that.setState({data : data, isView:true});
            }}>Lihat</Button>
            </td>
            <td>
              <Button className="btn btn-danger" onClick={()=>{
                that.toggleConfirmDeleteModal();
                that.setState({
                  itemToDelete: data
                })
              }}>Hapus</Button>
            </td>
          </tr>
        )
      })
    }else{
      return this.state.dataList.slice(0,9).map(function(data, index){
        return (
          <tr>
            <td className="text-center">
              <div>
                {index+1}
              </div>
            </td>
            <td>
              <div>{Moment(data.maintenance_date).format("DD-MM-YYYY")}</div>
            </td>
            <td className="text-center">
              <div>{data.maintenance_name}</div>
            </td>
            <td>
              <div><NumberFormat displayType={'text'} prefix={'Rp '} thousandSeparator={'.'} decimalSeparator={','} value={data.maintenance_cost}/></div>
            </td>
            <td className="text-center">
              <div>{data.operator_name}</div>
            </td>
            <td>
            <Button onClick={()=>{
              data.maintenance_date = Moment(data.maintenance_date, 'YYYY-MM-DD').toDate()
              that.setState({data : data, isView:true});
            }}>Lihat</Button>
            </td>
            <td>
              <Button className="btn btn-danger" onClick={()=>{
                that.toggleConfirmDeleteModal();
                that.setState({
                  itemToDelete: data
                })
              }}>Hapus</Button>
            </td>
          </tr>
        )
      })
    }

  }

  _afterSaveUpdateDelete(){
    var that = this;
    console.log("doing after effect");
    superagent
    .get(server+"maintenance?stockcard_id="+that.state.stockcard_id)
    .then(res => {
      if(res.body!=null){
        console.log("get result");
        that.setState({
          dataList : res.body,
          searchModal : false
        })
        var cost = 0;
        res.body.map(function(data, index){
          cost = cost + parseInt(data.maintenance_cost)
        });
        that.setState({
          total_maintenance : cost
        })
        const dataSets=[];
        const headerList =[];
        for(const i in res.body[0]){
          headerList.push([i]);
        }
        dataSets.push({
          dataset:that.state.dataList,
          sheet:'sheet 1',
          colNameList: headerList
        });
        that.setState({
          dataSets : dataSets
        })
        console.log("finish");
      }
    });

  }

  _renderSearchModalPickerData(){
    var that = this;
    return that.state.vehicleList.map(function(data, index){
      return (
        <tr onClick={()=>{
          {
            if(data.status_code==2){
              that.setState({ sold : true})
            } else {
              that.setState({ sold : false})
            }
            that.setState({
              stockcard_id : data.id,
              selected_vehicle : data.license_plate,
              selected_vehicle_price : data.purchase_price
            });
            superagent
            .get(server+"maintenance?stockcard_id="+data.id)
            .then(res => {
              if(res.body!=null){
                that.setState({
                  dataList : res.body,
                  searchModal : false
                })
                var cost = 0;
                res.body.map(function(data, index){
                  cost = cost + parseInt(data.maintenance_cost)
                });
                that.setState({
                  total_maintenance : cost
                })
                const dataSets=[];
                const headerList =[];
                for(const i in res.body[0]){
                  headerList.push([i]);
                }
                dataSets.push({
                  dataset:that.state.dataList,
                  sheet:'sheet 1',
                  colNameList: headerList
                });
                that.setState({
                  dataSets : dataSets
                })
              }
            });

        }}}
        >
          <td className="text-left">
            <div>
              {index+1}
            </div>
          </td>
          <td className="text-center">
            <div>{data.license_plate}</div>
          </td>
          <td className="text-center">
            <div>{data.car_name}</div>
          </td>
          <td className="text-center">
            <div>{data.car_color}</div>
          </td>
          <td className="text-center">
            <div>{data.tahun}</div>
          </td>
        </tr>
      )
    })
  }

  _renderSearchModal = () =>{
    return(
      <div>
        <Modal isOpen={this.state.searchModal} toggle={this.toggleSearchModal} className="modal-warning">
          <ModalHeader toggle={this.toggleSearchModal}>Pilih Kendaraan</ModalHeader>
          <ModalBody>
            <div className="table-wrapper-scroll-y my-custom-scrollbar">
            <Table hover responsive="sm" className="table-outline sb-0 mb-0 d-sm-table">
              <tbody>
              {this._renderSearchModalPickerData()}
              </tbody>
            </Table>
            </div>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleSearchModal}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    )
  }

  renderForm(){
    return(
      <Form>
        <FormGroup>
          <Label for="name">Nama Servis</Label>
          <Input  type="select" value={this.state.data.maintenance_name}  name="name" id="inputName" onChange={(e)=>{
            this.setState({
              data:{
                ...this.state.data,
                maintenance_name : e.target.value
              }
            })
          }} >
            {this.renderOption(this.state.maintenanceList)}
          </Input>
        </FormGroup>
        <FormGroup>
          <Label for="date">Tanggal Servis</Label>
          <br/>
          <DatePicker

            format={'yyyy-MM-dd'}
            onChange={(e)=>{
              this.setState({
                data : {
                  ... this.state.data,
                  maintenance_date: e
                }
              })
            }}
            value={this.state.data.maintenance_date}
          />
        </FormGroup>
        <FormGroup>
          <Label for="brand">Deskripsi</Label>
          <Input  value={this.state.data.description1} name="description1" id="inputDescription1" onChange={(e)=>{
            this.setState({
              data:{
                ...this.state.data,
                description1 : e.target.value
              }
            })
          }} />
          <br/>
          <Input  value={this.state.data.description2} name="description2" id="inputDescription2" onChange={(e)=>{
            this.setState({
              data:{
                ...this.state.data,
                description2 : e.target.value
              }
            })
          }} />
        </FormGroup>
        <FormGroup>
          <Row>
            <Col className="text-left pull-left" md={12} xs={12}>
              <Label className="text-left" for="biaya">Biaya</Label>

            </Col>
            <Col className="text-left pull-left" md={12} xs={12}>
              <NumberFormat  style={{width:'100%'}} value={this.state.data.maintenance_cost} thousandSeparator={'.'} decimalSeparator={','}
                prefix={'Rp '} onValueChange={(values) => {
                const {formattedValue, value} = values;
                // formattedValue = $2,223
                // value ie, 2223
                this.setState({
                  data :{
                    ... this.state.data,
                    maintenance_cost : value
                  }
                })
              }}/>
            </Col>
          </Row>
        </FormGroup>
        <FormGroup>
          <Label for="transmision">Nama Operator / Teknisi</Label>
          <Input  value={this.state.data.operator_name}  name="operatorName" id="inputOperatorName" onChange={(e)=>{
            this.setState({
              data:{
                ...this.state.data,
                operator_name : e.target.value
              }
            });
          }} />
        </FormGroup>
        <Row className="pull-right">
          <Col >
            <Button className="pull-right btn btn-success" onClick={()=>this._saveData()}>Simpan</Button>
          </Col>
        </Row>

      </Form>
    )
  }
}

export default Maintenance;
