import React, { Component, lazy, Suspense, useState } from 'react';
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Collapse,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Row,
  Table,
  Modal, ModalBody, ModalFooter, ModalHeader
} from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'

import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';
import ReactImageProcess from 'react-image-process';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Base from '../../views/Base/FormBase/Base';

const Widget03 = lazy(() => import('../../views/Widgets/Widget03'));

const brandPrimary = getStyle('--primary')
const brandSuccess = getStyle('--success')
const brandInfo = getStyle('--info')
const brandWarning = getStyle('--warning')
const brandDanger = getStyle('--danger')

class HomeGuest extends Base {
  constructor(props) {
    super(props);

    this.state = {
      url:"https://dianmobil.com/dealer/index.php/stockcard",
      viewModal:false,
      collapseContact:false,
      itemToView:{},
      dataList:[]
    };
    this.toggleViewModal = this.toggleViewModal.bind(this);
    this.toggleContact = this.toggleContact.bind(this);
  }

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  async _getDataList() {
    this._searchData();
  }

  componentWillMount() {
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
      var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
      s1.async=true;
      s1.src='https://embed.tawk.to/5f797e874704467e89f47af5/default';
      s1.charset='UTF-8';
      s1.setAttribute('crossorigin','*');
      s0.parentNode.insertBefore(s1,s0);
    })();
  }

  Slideshow = () => {
    return (
      <div style={{maxWidth: 554, margin: '0 auto'}}>
        <Carousel>
          <div>
            <img src={`https://dianmobil.com/dealer/uploads/images/${this.state.itemToView.car_images_1 || `../image-not-found.png`}`} />
          </div>
          <div>
            <img src={`https://dianmobil.com/dealer/uploads/images/${this.state.itemToView.car_images_2 || `../image-not-found.png`}`} />
          </div>
          <div>
            <img src={`https://dianmobil.com/dealer/uploads/images/${this.state.itemToView.car_images_3 || `../image-not-found.png`}`} />
          </div>
          <div>
            <img src={`https://dianmobil.com/dealer/uploads/images/${this.state.itemToView.car_images_4 || `../image-not-found.png`}`} />
          </div>
          <div>
            <img src={`https://dianmobil.com/dealer/uploads/images/${this.state.itemToView.car_images_5 || `../image-not-found.png`}`} />
          </div>
        </Carousel>
      </div>
    )
  }

  contactSection = () => {
    return (
      <div>
        <Button color="success" onClick={this.toggleContact} style={{width: '100%', marginBottom: 0}}><b>Tertarik ?</b></Button>
        <Collapse
          isOpen={this.state.collapseContact}
        >
          <Card>
            <CardBody>
              <p style={{marginBottom: '0.5rem'}}><b>Kontak 1</b></p>
              <a target="_blank" href="https://api.whatsapp.com/send?phone=6281381021352" className="btn btn-success" style={{width: '100%', marginBottom: '1rem'}}><img src={require('../../assets/img/icon-whatsapp.png')} style={{width: 16, marginRight: 5, marginTop: -4}}/>WhatsApp</a>
              <p style={{marginBottom: '0.5rem'}}><b>Kontak 2</b></p>
              <a target="_blank" href="https://api.whatsapp.com/send?phone=6281383975678" className="btn btn-success" style={{width: '100%', marginBottom: 0}}><img src={require('../../assets/img/icon-whatsapp.png')} style={{width: 16, marginRight: 5, marginTop: -4}}/>WhatsApp</a>
            </CardBody>
          </Card>
        </Collapse>
      </div>
    )
  }

  toggleContact() {
    this.setState({
      collapseContact: !this.state.collapseContact,
    });
  }

  toggleViewModal() {
    this.setState({
      viewModal: !this.state.viewModal,
    });
  }

  _renderViewModal = () =>{
    return(
      <div>
        <Modal isOpen={this.state.viewModal} toggle={this.toggleViewModal} className="modal-lg modal-success">
          <ModalHeader toggle={this.toggleViewModal}><b>Info Mobil</b></ModalHeader>
          <ModalBody>
            <Row>
              <Col xs={12} sm={{size: 10, offset: 1}}>
                {this.Slideshow()}
                <h3 style={{marginBottom: '1rem'}}><b>{this.state.itemToView.car_name}</b></h3>
                <Row>
                  <Col xs={12} sm={8}>
                    <p style={{padding: '1rem', marginBottom: 0, borderBottom: 'thin solid #555'}}><i class="fa fa-car" style={{marginRight: 10}}></i>Merek<span style={{float: 'right'}}>{this.state.itemToView.car_brand}</span></p>
                    <p style={{padding: '1rem', marginBottom: 0, borderBottom: 'thin solid #555'}}><i className="fa fa-calendar" style={{marginRight: 10}}></i>Tahun<span style={{float: 'right'}}>{this.state.itemToView.tahun}</span></p>
                    <p style={{padding: '1rem', marginBottom: 0, borderBottom: 'thin solid #555'}}><i className="fa fa-paint-brush" style={{marginRight: 10}}></i>Warna<span style={{float: 'right'}}>{this.state.itemToView.car_color}</span></p>
                    <p style={{padding: '1rem', marginBottom: 0, borderBottom: 'thin solid #555'}}><i className="fa fa-cog" style={{marginRight: 10}}></i>Transmisi<span style={{float: 'right'}}>{this.state.itemToView.car_transmision}</span></p>
                    <p style={{padding: '1rem', marginBottom: '1rem', borderBottom: 'thin solid #555'}}><i className="fa fa-tint" style={{marginRight: 10}}></i>Bahan bakar<span style={{float: 'right'}}>{this.state.itemToView.car_fuel}</span></p>
                  </Col>
                  <Col xs={12} sm={4}>
                    {this.contactSection()}
                  </Col>
                </Row>
              </Col>
            </Row>
          </ModalBody>
        </Modal>
      </div>
    )
  };

  render() {
    let that = this;
    const filterData = this.state.dataList.filter(function(item) {
      if (item.status_code === '1') return item
    });

    return (
      <div>
        <div className="custom-main-header">
          <div><img src={require('../../assets/img/logo.jpeg')} /></div>
          <div>
            <div><div style={{display: 'inline-block', width: 30, marginRight: 10, textAlign: 'right'}}><i class="fa fa-address-card-o"></i></div>Jl. Akses UI 28A</div>
            <div><div style={{display: 'inline-block', width: 30, marginRight: 10, textAlign: 'right'}}><i className="fa fa-location-arrow"></i></div>Depok</div>
            <div><div style={{display: 'inline-block', width: 30, marginRight: 10, textAlign: 'right'}}><i className="fa fa-phone"></i></div>(021) 8771 4646</div>
          </div>
        </div>
        <div className="" style={{marginTop: 155}}>
          {
            <div>
              {this._renderViewModal()}
            </div>
          }
          <Suspense fallback={this.loading()}>
            <div style={{margin: 24}}>
              <Grid container spacing={3}>
                {
                  filterData.map(function (data, index) {
                    return (
                      <Grid item xs={12} sm={6} md={4} lg={3}>
                        <Paper style={{boxShadow: 'none', backgroundColor: 'transparent'}}>
                          <div key={`key ${index}`} style={{
                              backgroundImage: 'linear-gradient(#5E3719, #B2A496)',
                              borderRadius: 20,
                              color: 'white',
                              cursor: 'pointer'
                            }}
                            onClick={() => {
                              that.toggleViewModal();
                              that.setState({
                                itemToView: data
                              })
                              console.log(data)
                            }}
                          >
                            <img
                              src={`http://dianmobil.com/dealer/uploads/images/${data.car_images_1 || `../image-not-found.png`}`}
                              style={{width: '100%', borderTopLeftRadius: 20, borderTopRightRadius: 20}}/>
                            <div style={{padding: 20}}>
                              <h3><b>{data.car_name + ' - ' + data.tahun}</b></h3>
                              <Row>
                                <Col xs="6"><p style={{marginBottom: 0, fontWeight: 600}}>{data.car_brand}</p></Col>
                                <Col xs="6"><p
                                  style={{marginBottom: 0, fontWeight: 600, textAlign: 'right'}}>{data.car_color}</p></Col>
                              </Row>
                            </div>
                          </div>
                        </Paper>
                      </Grid>
                    )
                  })
                }
              </Grid>
            </div>
          </Suspense>
          <div style={{width: '100%', textAlign: 'center', padding: 20}}>
            Copyright 2020
          </div>
        </div>
      </div>
    );
  }
}

export default HomeGuest;
