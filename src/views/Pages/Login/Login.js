import React, { Component } from 'react';
import { Link , Redirect, Route, Switch } from 'react-router-dom';

import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';

import superagent from 'superagent';


import routes from '../../../routes';

class Login extends Component {

  constructor(props){
    super(props);

    if(localStorage.getItem("username")!==null){
      this.props.history.push('/');
    }

    this.state={
      username:'',
      password:''
    }
  }

  state = {
    username : '',
    password : ''
  }

  handleChangeUsername(e) {
    this.setState({ username: e.target.value });
  }

  handleChangePassword(e) {
    this.setState({ password: e.target.value });
  }

  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>

          <Row className="justify-content-center">
            <Col md="5">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form>
                      <h1>Login</h1>
                      <p className="text-muted">Sign In to your account</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" onChange={this.handleChangeUsername.bind(this)} value={this.state.username} placeholder="Username" autoComplete="username" />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" onChange={this.handleChangePassword.bind(this)} value={this.state.password} placeholder="Password" />
                      </InputGroup>
                      <Row>
                        <Col xs="3">
                          <Button color="primary" className="px-4" onClick={()=>{
                            superagent
                            .get("https://dianmobil.com/dealer/index.php/user/")
                            .query({usernm : this.state.username})
                            .query({passwd : this.state.password})
                            .then(res =>{
                              if(res.body!=null){
                                if(res.body.length>0){
                                  localStorage.setItem('username',res.body[0].usernm);
                                  localStorage.setItem('role',res.body[0].role);
                                  console.log(res.body[0].role);
                                  this.props.history.push('/');
                                }else{
                                  window.alert("Password salah");
                                }
                              }
                            });
                          }}>Login</Button>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
