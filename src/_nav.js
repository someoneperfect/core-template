

export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
      badge: {
        variant: 'info',
        text: 'NEW',
      },
    },
    {
      name: 'Stock',
      url: '/stock',
      icon: 'icon-puzzle'
    },
    {
      name: 'Maintenance',
      url: '/maintenance',
      icon: 'icon-settings'
    },
    {
      name: 'Transaction',
      url: '/transaction',
      icon: 'icon-calculator'
    },
    {
      name: 'Report',
      url: '/report',
      icon: 'icon-note'
    },
    {
      name: 'Log Out',
      url: '/logout',
      icon: 'fa fa-lock',
      attributes: {
        onClick: e => {
          e.preventDefault();
          localStorage.removeItem('username');
          localStorage.removeItem('role');
          window.location.href="/"
        }
      }

    }
  ],
};
