import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { Badge, Button, UncontrolledDropdown, DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem } from 'reactstrap';
import PropTypes from 'prop-types';

import { AppAsideToggler, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import logo from '../../assets/img/logo.png'
import sygnet from '../../assets/img/brand/sygnet.svg'

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
  render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: logo, width: 157, height: 55, alt: 'PT. Dian Mobil Logo' }}
          minimized={{ src: sygnet, width: 30, height: 30, alt: 'CoreUI Logo' }}
        />

        <Nav className="ml-auto" navbar mobile>
          <NavItem className="d-md-down-none">
            <NavLink to="#" className="nav-link"><i className="icon-user"></i></NavLink>
          </NavItem>
          <NavItem className="d-md-down-none">
            <Button onClick={e => this.props.onLogout(e)} className="nav-link"><i className="fa fa-lock"></i></Button>
          </NavItem>
        </Nav>
        {/*<AppAsideToggler className="d-lg-none" mobile />*/}
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
