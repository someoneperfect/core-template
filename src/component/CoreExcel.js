import React from "react";
import ReactExport from "react-export-excel";
import {
  Button
} from 'reactstrap';

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

export default class CoreExcel extends React.Component {
    render() {
        return (
            <ExcelFile element={
              <div className="pull-left mb-3">
                <Button className="mr-2 btn btn-success">{this.props.buttonText}</Button>
              </div>
            }>
                {
                  this.props.datasets && this.props.datasets.map((value)=>{
                    return(
                      <ExcelSheet data={value.dataset} name={value.sheet}>
                          {
                            value.colNameList.map((data)=>{
                              return(
                                <ExcelColumn label={data} value={data}/>
                              )
                            })
                          }
                      </ExcelSheet>
                    )
                  })
                }
            </ExcelFile>
        );
    }
}
